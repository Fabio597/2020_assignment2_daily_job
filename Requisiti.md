## Requisiti

Sono stati individuati i seguenti _Requisiti Funzionali_ dopo la somministrazione del questionario. L'attività di valutazione delle risposte ha portato alla definizione dei seguenti requisiti:

#### Requisiti Utilizzatore

1. Mostrare il prezzo indicativo dei lavoratori, stimato come prezzo all'ora, da poter visualizzare quando compare la lista dei candidati per una determinata mansione.
2. Mostrare la specializzazione del lavoratore all'interno dell'annuncio per far capire a chi sta offrendo il lavoro quanto il candidato sia esperto nel lavoro da svolgere.
3. Il candidato deve poter essere contattato da chi offre il lavoro attraverso: Numero di Telefono, E-Mail e Chat.
4. Non deve essere necessario specificare con esattezza la data e l'ora dell'inizio della prestazione lavorativa.

#### Requisiti Prestatore

5. Devono essere mostrate le offerte di lavoro più vicine all'abitazione di chi sta cercando lavoro.
6. Deve essere possibile vedere quanto (indicativamente) è retribuito un lavoro, prima di potersi candidare effettivamente.
7. Mostrare attraverso una notifica e con l'invio di una E-Mail la pubblicazione di un nuovo lavoro nelle vicinanze o di una mansione che si è interessati a svolgere.
8. Deve essere possibile segnalare in fase di candidatura, e successivamente, eventuali problematiche attraverso una di queste modalità: Chat interna all'app, Casella di testo o E-Mail.
9. Gli annunci devono poter essere visualizzati sia attraverso un elenco che attraverso una mappa geografica.
10. La lista dei lavori deve poter essere ordinata e/o filtrata secondo queste criteri: Vicinanza (è l'ordinamento di default anche secondo il requisito numero 5), Retribuzione e Lavoro (Filtro applicato al tipo di lavoro).

#### Requisiti Generali

11. Deve essere possibile scrivere una recensione per entrambe le parti, alla fine della prestazione lavorativa.
12. Deve essere possibile poter visualizzare le recensioni di un altro profilo in fase di selezione (per chi offre il lavoro) e in fase di candidatura (chi sta cercando lavoro).
13. Deve poter essere possibile accedere all'applicazione attraverso queste modalità: Credenziali e E-Mail.
14. L'applicazione deve essere disponibile gratuitamente ma con la visione di messaggi pubblicitari visualizzati tramite Banner.