##  Questionario
Il questionario che andiamo a definire ci permette di ricavare una serie di informazioni che saranno utili per comprendere i requisiti che il nostro sistema software dovrà rispettare. Le domande sono state studiate e organizzate per facilitare la definizione delle priorità, per lo sviluppo delle funzionalità e per eventuali sviluppi che potranno esserci in futuro.
Il questionario è rivolto agli utenti finali del sistema perchè sono proprio loro che andranno ad utilizzare l'applicazione finale. Abbiamo selezionato un campione della popolazione di stakeholder a cui poter somministrare il questionario e ricavare in seguito le informazioni che stiamo cercando.

### Questionario: Struttura e Domande
In questa sezione illustreremo la struttura generale del questionario e le domande che vengono poste nello specifico. Abbiamo suddiviso il questionario in tre macroaree principali in modo da ricavare poi i requisiti necessari per la validazione e in futuro lo sviluppo dell'applicativo software.

#### Struttura
La struttura del questionario (vedi immagine riportata di seguito) è definita in modo dinamico. Abbiamo introdotto alcune domande che ci permettono di veicolare la visualizzazione delle sezioni (gruppi di domande) in base alle risposte che vengono date dall'intervistato:
1. __Chi cerca lavoro__: Sezione che mostra domande per chi cerca/potrebbe cercare un lavoro occasionale, quindi ci riferiamo al _Prestatore_
2. __Chi potrebbe offrire lavoro__: Sezione che mostra le domande per chi potrebbe essere interessato ad offrire un lavoro occasionale, quindi ci riferiamo all'_Utilizzatore_

![image info](./Questionario.png)

#### Domande
Per facilitare la comprensione delle domande, è stata scritta una breve introduzione prima dell'inizio della compilazione del questionario (nella schermata principale).

##### Offerire lavoro (domanda per capire l'interessamento)

*Ti è mai capitato, o pensi di avere necessità in futuro, di cercare qualcuno per svolgere un lavoro occasionale?(Imbianchino/Elettricista/Ripetizioni/Baby Sitter/Dog Sitter/...)*: Questa prima domanda ci serve sostanzialmente per veicolare il flusso del questionario, mostrando solo le domande a cui uno stakeholder è interessato.
Se uno stakeholder a questa domanda dovesse rispondere __si__ allora gli/le verrà mostrato il set di domande creato apposta per gli stakeholder che sono interessati (ora o in futuro) ad offrire un lavoro occasionale. Nel caso in cui venga data risposta __no__ allora passeremo alla prossima sezione.
Sono stati inoltre inseriti alcuni esempi di lavori che solitamente possono essere svolti occasionalmente per riuscire a chiarire meglio il dominio su cui ci stiamo muovendo, senza però dare limiti ai tipi di lavori che si potranno offrire.

##### Offerire lavoro
*Quanto è importante sapere il costo (indicativo) di una determinata mansione prima di contattare il lavoratore?*: Questa domanda è stata posta per capire quanto un potenziale offerente è intenzionato a conoscere prima il costo per svolgere un lavoro. Nella domanda è stato specificato intenzionalmente: __"prima di contattare il lavoratore"__, perchè è necessario capire se questa informazione potrebbe essere visualizzata in fase di selezione del candidato (quando più di un candidato si è offerto per svolgere la mansione di cui ho bisogno) oppure se le due parti potrebbero essere più interessate a discuterne in seguito durante un incontro.
Per la valutazione abbiamo scelto una scala da 1 a 5 (1 = poco importante e 2 = molto importante)

*Quanto è importante la specializzazione del lavoratore in relazione al costo?*: Questa domanda è stata pensata per capire quanto gli stakeholder sono intenzionati a valutare sia il costo, sia la specializzazione del lavoratore in una determinata mansione. Questo perchè nel nostro dominio di interesse sono presenti anche, se non soprattutto, persone non qualificate che potrebbero candidarsi per svolgere un lavoretto ma magari ad un prezzo inferiore rispetto ad un professionista. Un esempio potrebbe esserci una persona che cerca qualcuno per farsi aiutare ad imbiancare casa, senza chiamare un imbianchino professionista (ad un costo più elevato) ma, ad esempio, un ragazzo che non è esperto in questo lavoro (ad un costo sicuramente inferiore).
Inoltre questa domanda ci permette di capire se un eventuale ordine delle offerte fatte dai candidati in base al rapporto costo/specializzazione può essere una buona visualizzazione da parte dell'offerente.
Per la valutazione abbiamo scelto una scala da 1 a 5 (1 = poco importante e 2 = molto importante)

*Una volta scelto il candidato tra quelli proposti dall'applicazione, come preferiresti contattarlo*: Con questa domanda ci aspettiamo di capire quali sono le modalità preferite dagli utenti per contattare un candidato e fissare un primo incontro, se non cominciare direttamente il lavoro. A questa domanda si possono dare più risposte perchè teniamo in considerazione più modi per svolgere questa funzionalità, ma prenderemo di più in considerazione le modalità che il campione di stakeholder ha selezionato (E-Mail, Numero di telefono, Chat all'interno dell'applicazione o altro).

*Preferiresti accordarti in seguito su data e ora del lavoro o fissarli anticipatamente?*: Questa domanda è stata posta per comprendere in che modo un possibile stakeholder che vuole offrire un lavoro vuole iniziare a fissare una data per l'inizio dei lavori. In questo caso ci aspettiamo di capire se è necessario poter specificare nell'applicazione un giorno e un orario (indicativamente) per far capire tramite l'annuncio se ci sono possibilità di accordarsi tramite un incontro per l'inizio dei lavori oppure se oltre quella data non si può andare. Questo requisito può indirettamente riguardare anche lo stakeholder che invece sta cercando un lavoro, perchè può verificare la sua disponibilità prima di candidarsi.
In questa domanda la risposta che si può dare è una tra le due disponibili: _Data e ora fissati_ & _Accordo in seguito_

*In che modo preferiresti che il pagamento venisse effettuato?*: Con questa domanda, principalmente, vogliamo capire se gli stakeholder preferirebbero effettuare il pagamento tramite l'applicazione, quindi tracciando anche il pagamento oppure accordandosi tra loro senza lasciare all'app il compito di tracciare il pagamento. Questo tipo di requisito risulta abbastanza delicato perchè potrebbero esserci mancati saldi e/o pagamenti in nero, nel caso in cui l'app non si facesse carico di effettuare questa operazione. Questo argomento verrà trattato in seguito, ma nel caso in cui la maggior parte delle risposte siano direzionate verso il __pagamento indipendente dall'App__, lasceremo lo scopo del software solo all'incontro delle due parti. Le possibili risposte sono quindi:
	1. Pagamento tramite App
	2. pagamento indipendente dall'App

##### Cercare un lavoro (domanda per capire l'interessamento)

*Ti piacerebbe avere l'opportunità di guadagnare qualcosa se ti proponessero un lavoro occasionale?*: Questa domanda, analogamente alla prima, è stata posta per veicolare il contenuto del questionario in modo da mostrare solo agli stakeholder interessati la sezione riguardante le domande per chi è interessato a cercare un lavoro occasionale. Come descritto nella prima domanda, in caso di risposta: __si__ mostreremo la sezione, mentre con risposta __no__ passeremo alla sezione finale riguardante le domande generiche.

##### Cercare un lavoro

*Quanto sarebbe importante per te trovare un lavoro entro una certa distanza dalla tua abitazione/luogo in cui vivi?*: Questa domanda ci permette di capire quanto per lo stakeholder sia importante la distanza del luogo in cui verrà svolta la mansione rispetto al luogo in cui vive. Infatti nel dominio possiamo trovare vari tipi di utenti, per alcuni la distanza (entro i dovuti limiti) può non essere un problema, ad esempio nel momento in cui si dispone di un auto, mentre per altri può essere difficile accettare di svolgere una mansione in un luogo anche non troppo distante, ad esempio uno studente che può spostarsi solo a piedi. Inoltre potrebbe essere un possibile parametro iniziale su cui basiamo la visualizzazione dei lavori. Per la valutazione è stata scelta una scala da 1 a 5 (1=poco importante, 5=molto importante).

*Quanto reputi importante sapere indicativamente la retribuzione prima di candidarti per un lavoro?*: Il motivo per cui viene posta questa domanda è l'interesse nel comprendere quanto la retribuzione sia una priorità per il lavoratore. Potrebbero esserci più casi, ad esempio per uno studente che svolge ripetizioni ci si attende una retribuzione tendenzialmente standard, quindi in questo caso l'importanza potrebbe essere bassa, invece nel caso di un lavoratore altamente specializzato, come ad esempio un elettricista che deve svolgere un lavoro delicato l'importanza sarà più alta. Inoltre ci serve per capire se potrebbe essere un potenziale dato da aggiungere durante la fase di pubblicazione dell'offerta lavorativa (da chi offre il lavoro), così da poter essere visualizzata dai possibili candidati. Per la valutazione la scala va da 1 a 5 (1=poco importante, 5=molto importante).

*Come vorresti essere aggiornato sulle offerte di lavoro nella tua zona?*: Questa domanda ci permette di capire quali sono le modalità preferite dagli stakeholder per essere aggiornati su eventuali offerte di lavoro. Le possibili risposte sono 3:1.E-Mail 2.SMS 3.Sezione apposita dell'App(o notifica) oppure c'è la possibilità di inserire in una casella di testo un'altra opzione. Ognuna di queste risposte ci darà informazioni importanti riguardo al funzionamento del sistema, specie su come visualizzare il profilo dell'utente.

*Come vorresti segnalare eventuali problematiche? (Allergie,Handicap,...)*: La domanda viene posta per permettere agli utenti che presentano problematiche meno comuni di specificare come comunicare l'informazione all'eventuale offerente, evitando spiacevoli inconvenienti. Abbiamo pensato di inserire questa domanda per agevolare anche quelle categorie di persone che hanno maggiori problemi nello svolgere certe azioni (ad esempio allergie ad animali oppure l'impossibilità di salire le scale,...). Le possibili risposte sono 1.Chat all'interno dell'App 2.Casella di testo 3.E-Mail.

*Come vorresti visualizzare all'interno dell'applicazione gli annunci?*: Il motivo della domanda è comprendere in quale modo gli stakeholder vogliono visualizzare le offerte, ovvero secondo una mappa (eg.TripAdvisor, Booking) quindi senza criterio di ordinamento oppure tramite Lista, in questo caso verrà mostrata un'ulteriore sezione del questionario. Non essendoci un criterio di ordinamento nel caso della mappa, sarà necessario specificare quali tipi di ordinamento sono più importanti per i nostri utenti (vedi domanda successiva) ma solo riferendosi alla lista. Le risposte possibili sono: 1.Mappa 2.Elenco/Lista 3.Entrambi.

*Quali sono per te i criteri di ordinamento/filtro più importanti nella visualizzazione degli annunci?*: Questa domanda consente di comprendere quale sarà/quali saranno i criterii di ordinamento utilizzati, quindi ad esempio nel caso di "Retribuzione" il primo risultato della lista corrisponderà al pagamento più alto e l'ultimo al più basso. Questo requisito sarà particolarmente importante per il fine dell'applicazione, cioè l'immediatezza nello scegliere l'offerta migliore per l'utente. Inoltre le risposte potranno essere dei possibili ordinamenti/filtri da sviluppare e da applicare alla lista dei lavori.
Sarà possibile scegliere una o più opzioni tra 1.Vicinanza 2.Lavoro 3.Retribuzione 4.Specificare altro.

#### Domande Generali

*Quanta importanza dai alla possibilità di scrivere una recensione su un altro profilo?*: Questa domanda fornisce informazioni riguardo l'implementazione nell'App della possibilità di scrivere recensioni, in caso di una maggioranza di risposte con punteggio alto sarà possibile sfruttare questa funzionalità, altrimenti non verrà sviluppata. Questa domanda è anch'essa un po delicata, nel senso che non sempre le persone sono intenzionate a lasciare recensioni. Come tutte le domande di questa sezione è rivolta sia a chi cerca il lavoro sia a chi offre la possibilità di svolgere una mansione. Come si può intuire, le recensioni per ogni profilo possono essere di due tipi: _Recensioni Prestatore_ e _Recensioni Utilizzatore_ dato che una persona può candidarsi per un lavoro e magari in un altro momento offrire un lavoro (verrà ripreso in anche seguito).
La scala va da 1 a 5 (1=poco importante 5=molto importante)

*Quanta importanza dai alle recensioni altrui di solito?*: Questo caso può sembrare simile al precedente, ma mentre nel primo la domanda riguardava una funzionalità aggiuntiva, questo quesito ci permetterà di capire quanto importanza dare agli utenti con recensioni migliori rispetto a quelli con recensioni peggiori, specie nel caso in cui siano equivalenti rispetto ai criteri di ordinamento. Inoltre ci farà capire quanto gli utenti siano interessati a leggere recensioni altrui prima di candidarsi e/o scegliere un candidato.
Sarà possibile scegliere un valore da 1 a 5 (1=poco importante 5=molto importante)

*Come vorresti accedere all'applicazione?*: Requisito rigurdante la modalità di accesso all'applicazione, utile per comprendere quali tipi di account l'utente preferirebbe utilizzare, quindi ad esempio la E-Mail personale, più efficiente per la comunicazione o i social network, più immediati per effettuare una prima registrazione. 
Le possibili risposte sono 1.Social Network 2.E-Mail 3.Credenziali 4.Specificando un'altra opzione.

*Preferiresti pagare l'applicazione (piccolo contributo) oppure avere accesso gratuito ma con la visione di inserzioni pubblicitarie?*: La risposta alla domanda ramificherà un'ultima volta il questionario, portando in entrambi i casi alla domanda conclusiva. In questo caso ci interessa anche capire se la maggior parte degli utenti preferisce dare un piccolo contributo oppure usufruire gratuitamente ma con la visione di inserzioni pubblicitarie. 
Le scelta ricadrà su 1.Pagamento 2.Pubblicità

*In che formato preferiresti vedere i messaggi pubblicitari?*: In questo caso il potenziale utente avrà scelto "Pubblicità", si dovrà quindi decidere il formato, la risposta con più voti andrà a modificare il mockup dell'applicazione, ci aspettiamo che la maggior parte di queste risposte saranno date dal lavoratore, poichè meno propenso a spendere. 
Sarà possibile scegliere tra 1.Banner 2.Video.

*Come preferiresti pagare l'applicazione?*: In questo caso il potenziale utente avrà scelto "Pagamento" sarà quindi necessario scgliere la modalità di pagamento della applicazione al momento del download dallo Store, è molto probabile che le risposte daranno una risposta a questa domanda gli utenti che offrono la possibilità di svolgere una mansione. 
Le eventuali risposte saranno 1.Carta di credito 2.Credito telefonico 3.Google Pay/Apple Pay/simili.

Link Questionario: https://docs.google.com/forms/d/1KTQjkQ8yhAn6Bs3okwtmvZ8_FYFzBcQID1gG1jw6xJ8/edit#responses

