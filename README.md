# Daily Job (Assignment 2 - Elicitation)

# Sommario
1. Descrizione Progetto
2. Identificazione degli Stakeholder
3. Background study (system-as-is)
4. Questionario
5. Risposte al Questionario
6. Requisiti
7. Partecipanti



## Descrizione Progetto

Il nostro progetto consiste nel realizzare un'applicazione chiamata **Daily Job**, il cui scopo è quello di permettere agli utenti di sfruttare il proprio tempo libero e trasformarlo in una possibilità di guadagno. L'idea è di offrire l'opportunità di un lavoro occasionale a chi ne ha bisogno, andando a facilitare la fase di ricerca e comunicazione con chi ha necessità di una qualche forma di manodopera. Il sistema quindi va ad agevolare il matching tra domanda e offerta per lavori occasionali. L'intento è quindi far incontrare le due parti per poi lasciare a loro le modalità che preferiscono per accordarsi effettivamente sugli aspetti concreti dell'erogazione del servizio.
L'applicazione sarà quindi in grado di:

* Mostrare i vari lavori disponibili in una certa provincia e/o in generale in una certa zona Geografica, consentendo di visualizzarli attraverso alcuni criteri.
* Permettere a chi ha bisogno di ricevere una prestazione lavorativa di specificare la mansione, il giorno e la fascia oraria.
* Consentire a chi vorrebbe fare un lavoretto di poter cercare tra le eventuali offerte presenti.
* Chi poi risulta effettivamente interessato ad uno dei lavori presenti può, sempre attraverso l'applicazione, candidarsi per erogare effettivamente il servizio.

Sosteniamo come requisito fondamentale l'affidabilità e la sicurezza, motivo per cui viene implementato un sistema di recensioni simile ad altri servizi già diffusi (ex. TripAdvisor).

Questo documento si pone come obbiettivo quello di stabilire quali requisiti questo sistema deve soddisfare per garantire la corretta interpretazione e lo sviluppo del sistema da sviluppare.



## Identificazione Stakeholder

Per questa attività, le categorie di __StakeHolder__ individuate sono due:

* __Gli sviluppatori dell'applicazione__: Vengono citati come Stakeholder, e di conseguenza inclusi in questa fase, ma non saranno oggetto di intervista/questionario perchè prenderemo in considerazione in modo particolare i requisiti che verranno ricavati dall' intervista/questionario degli stakeholder che realmente saranno gli utilizzatori del sistema applicativo (utenti).
* __Utenti__: Ci riferiamo agli utenti finali che utilizzeranno effettivamente l'applicazione Daily Job e in questo caso possiamo fare una distinzione in due sottocategorie:
  * __Utenti che offrono la prestazione lavorativa__:  Con questa prima categoria identifichiamo le persone che potrebbero offrire una prestazione lavorativa di qualsiasi tipo. Ovviamente la nostra applicazione riguarda più che altro lavori che non richiedono sempre una specializzazione (Imbianchino/Dog Sitter/Baby Sitter/Ripetizioni,....), inoltre pensiamo sia un contesto ricorrente al giorno d'oggi.
  * __Utenti che richiedono la mansione__: Con questa seconda categoria invece andiamo ad identificare  le persone che davanti ad un'opportunità di lavoro occasionale, magari per incassare qualche guadagno extra, accetterebbero alcune tipologie di lavoro.

In aggiunta, è opportuno specificare che un potenziale Stakeholder della nostra applicazione, in particolare tra gli __Utenti__, potrebbe far parte di entrambe le sottocategorie citate sopra. Anche se in linea generale, ci aspettiamo che la seconda sottocategoria di __Utenti__ sarà costituita principalmente da studenti o lavoratori part-time e in parte minore da lavoratori full-time che hanno tempo libero nel weekend.
E' quindi lecito stimare un'età media compresa tra i 20 e i 35 anni nella maggior parte dei casi.
Per la prima sottocategoria di utenti invece possiamo aspettarci un'età media superiore ai 40 anni, con possibilità economiche maggiori della seconda.



## Background study

In questa sezione ci concentreremo a sottolineare gli aspetti e le modalità che caratterizzano oggi i lavori giornalieri e quali sono i mezzi più comuni e utilizzati per mettere in contatto persone che sono alla ricerca di un lavoro giornaliero e chi necessita di questo servizio.

Per prima cosa è possibile chiedersi: __Chi cerca un lavoro giornaliero o saltuario come può trovarlo?__ e __Chi ha bisogno di un servizio come fa a trovare a chi rivolgersi?__. Principalmente sono state individuate due modalità che ad oggi sono abbastanza utilizzate:

* L'utilizzo di __biglietti/volantini__ da affiggere in giro in cui tendenzialmente viene descritto brevemente il lavoro che si è disposti a svolgere. Stampato su esso viene lasciato anche il numero di telefono dell'emittente. (esempi di lavori ricercati in questo modo sono: Ripetizioni, Imbianchini, Baby Sitter, Dog Sitter, ecc...).
* Un'altra modalità ricorrente oggi è il __passaparola__, ovvero quando si cerca qualcuno per un lavoro (come possono essere quelli elencati nel punto precedente) tramite conoscenze è possibile trovare una persona in grado di svolgere quel determinato lavoro.

### Scenari system-as-is

Qui di seguito mostriamo due *scenari* per chiarire le due modalità citate in precedenza:

Scenario __volantini__:

1. Un signore ha bisogno di imbiancare due locali del proprio appartamento.
2. Mentre aspettava l'autobus ha notato un volantino con attaccati dei numeri di telefono di un signore che si offre per svolgere lavori come idraulico/imbianchino/elettricista per arrotondare lo stipendio.
3. L'interessato stacca un bigliettino con sopra indicato il numero di telefono oppure salva direttamente il suo numero.
4. Sempre l'interessato chiama il signore e cerca di mettersi d'accordo per incontrarsi e discutere su una possibile offerta.

Scenario __passaparola__:

1. Un mamma ha bisogno di cercare qualche studente, possibilmente delle scuole superiori, per fare ripetizioni in Matematica a suo figlio.
2. Parlando con qualche sua amica, le viene consigliato un ragazzo che ha già dato ripetizioni  proprio al figlio della sua amica.
3. La stessa amica in questo caso passa il contatto telefonico alla mamma che ha bisogno di contattare il ragazzo che potrebbe fare ripetizioni a suo figlio.
4. La mamma in seguito chiamerà questo ragazzo per chiedere la sua disponibilità per fare ripetizioni a suo figlio, accordandosi direttamente tramite telefono, oppure programmando un incontro.


##  Questionario
Il questionario che andiamo a definire ci permette di ricavare una serie di informazioni che saranno utili per comprendere i requisiti che il nostro sistema software dovrà rispettare. Le domande sono state studiate e organizzate per facilitare la definizione delle priorità, per lo sviluppo delle funzionalità e per eventuali sviluppi che potranno esserci in futuro.
Il questionario è rivolto principalmente agli utenti finali del sistema perchè sono proprio loro che andranno ad utilizzare l'applicazione finale. Abbiamo selezionato un campione della popolazione di stakeholder a cui poter somministrare il questionario e ricavare in seguito le informazioni che stiamo cercando.

### Questionario: Struttura e Domande
In questa sezione illustreremo la struttura generale del questionario e le domande che vengono poste nello specifico. Abbiamo suddiviso il questionario in tre macroaree principali in modo da ricavare poi i requisiti necessari per la validazione e in futuro lo sviluppo dell'applicativo software.

#### Struttura
La struttura del questionario (vedi immagine riportata di seguito) è definita in modo dinamico. Abbiamo introdotto alcune domande che ci permettono di veicolare la visualizzazione delle sezioni (gruppi di domande) in base alle risposte che vengono date dall'intervistato:
1. __Chi cerca lavoro__: Sezione che mostra domande per chi cerca/potrebbe cercare un lavoro occasionale.
2. __Chi potrebbe offrire lavoro__: Sezione che mostra le domande per chi potrebbe essere interessato ad offrire un lavoro occasionale.

![image info](./Questionario.png)

#### Domande
Per facilitare la comprensione delle domande, è stata scritta una breve introduzione prima dell'inizio della compilazione del questionario (nella prima schermata).

##### Offerire lavoro (domanda per capire l'interessamento)

*Ti è mai capitato, o pensi di avere necessità in futuro, di cercare qualcuno per svolgere un lavoretto occasionale?(Imbianchino/Elettricista/Ripetizioni/Baby Sitter/Dog Sitter/...)*: Questa prima domanda ci serve sostanzialmente per veicolare il flusso del questionario, mostrando solo le domande a cui uno stakeholder è interessato.
Se uno stakeholder a questa domanda dovesse rispondere __si__ allora potremo mostrargli il set di domande creato apposta per gli stakeholder che sono interessati (ora o in futuro) ad offrire un lavoro occasionale. Nel caso in cui venga data risposta __no__ allora passeremo alla prossima sezione.
Sono stati inoltre inseriti alcuni esempi di lavori che solitamente possono essere svolti occasionalmente per riuscire a chiarire meglio il dominio su cui ci stiamo muovendo, senza però dare limiti ai tipi di lavori che si potranno offrire.

##### Offerire lavoro
*Quanto è importante sapere il costo (indicativo) di una determinata mansione prima di contattare il lavoratore?*: Questa domanda è stata posta per capire quanto un potenziale offerente è intenzionato a conoscere prima il costo per svolgere un lavoro. Nella domanda è stato specificato intenzionalmente: __"prima di contattare il lavoratore"__, perchè è necessario capire se questa informazione potrebbe essere visualizzata in fase di selezione del candidato (quando più di un candidato si è offerto per svolgere la mansione di cui ho bisogno) oppure se le due parti potrebbero essere più interessate a discuterne in seguito durante un incontro.

*Quanto è importante la specializzazione del lavoratore in relazione al costo?*: Questa domanda è stata pensata per capire quanto gli stakeholder sono intenzionati a valutare sia il costo, sia la specializzazione del lavoratore in una determinata mansione. Questo perchè nel nostro dominio di interesse sono presenti anche, se non soprattutto, persone non qualificate che potrebbero candidarsi per svolgere un lavoretto ma magari ad un prezzo inferiore rispetto ad un professionista. Un esempio potrebbe esserci una persona che cerca qualcuno per farsi aiutare ad imbiancare casa, senza chiamare un imbianchino professionista (ad un costo più elevato) ma, ad esempio, un ragazzo che non è esperto in questo lavoro (ad un costo sicuramente inferiore).
Inoltre questa domanda ci permette di capire se un eventuale ordine delle offerte fatte dai candidati in base al rapporto costo/specializzazione può essere una buona visualizzazione da parte dell'offerente.
Per la valutazione abbiamo scelto una scala da 1 a 5 (1 = poco importante e 2 = molto importante)

*Una volta scelto il candidato tra quelli proposti dall'applicazione, come preferiresti contattarlo*: Con questa domanda ci aspettiamo di capire quali sono le modalità preferite dagli stakeholder per contattare un candidato e fissare un primo incontro se non cominciare direttamente il lavoro. A questa domanda si possono dare più risposte perchè teniamo in considerazione più modi per svolgere questa funzionalità, ma prenderemo di più in considerazione le modalità che il campione di stakeholder ha selezionato (E-Mail, Numero di telefono, Chat all'interno dell'applicazione o altro).

*Preferiresti accordarti in seguito su data e ora del lavoro o fissarli anticipatamente?*: Questa domanda è stata posta per comprendere in che modo un possibile stakeholder che vuole offrire un lavoro vuole iniziare a fissare una data per l'inizio dei lavori. In questo caso ci aspettiamo di capire se è necessario poter specificare nell'applicazione un giorno e un orario (indicativamente) per far capire tramite l'annuncio se ci sono possibilità di accordarsi tramite un incontro per l'inizio dei lavori oppure se oltre quella data non si può andare. Questo requisito può indirettamente riguardare anche lo stakeholder che invece sta cercando un lavoro, perchè può verificare la sua disponibilità prima di candidarsi.
In questa domanda la risposta che si può dare è una tra le due disponibili: _Data e ora fissati_ & _Accordo in seguito_

*In che modo preferiresti che il pagamento venisse effettuato?*: Con questa domanda, principalmente, vogliamo capire se gli stakeholder preferirebbero effettuare il pagamento tramite l'applicazione, quindi tracciando anche il pagamento oppure accordandosi tra loro senza lasciare all'app il compito di tracciare il pagamento. Questo tipo di requisito risulta abbastanza delicato perchè potrebbero esserci mancati saldi e/o pagamenti in nero, nel caso in cui l'app non si facesse carico di effettuare questa operazione. Questo argomento verrà trattato in seguito, ma nel caso in cui la maggior parte delle risposte siano direzionate verso il __pagamento indipendente dall'App__, lasceremo lo scopo del software solo all'incontro delle due parti. Le possibili risposte sono quindi:
	1. Pagamento tramite App
	2. pagamento indipendente dall'App

##### Cercare un lavoro (domanda per capire l'interessamento)

*Ti piacerebbe avere l'opportunità di guadagnare qualcosa se ti proponessero un lavoro occasionale?*: Questa domanda, analogamente alla prima, è stata posta per veicolare il contenuto del questionario in modo da mostrare solo agli stakeholder interessati la sezione riguardante le domande per chi è interessato a cercare un lavoro occasionale. Come descritto nella prima domanda, in caso di risposta: __si__ mostreremo la sezione, mentre con risposta __no__ passeremo alla sezione finale riguardante le domande generiche.

##### Cercare un lavoro

*Quanto sarebbe importante per te trovare un lavoro entro una certa distanza dalla tua abitazione/luogo in cui vivi?*: Questa domanda ci permette di capire quanto per lo stakeholder sia importante la distanza del luogo in cui verrà svolta la mansione rispetto al luogo in cui vive. Infatti nel dominio possiamo trovare vari tipi di utenti, per alcuni la distanza (entro i dovuti limiti) può non essere un problema, ad esempio nel momento in cui si dispone di un auto, mentre per altri può essere difficile accettare di svolgere una mansione in un luogo anche non troppo distante, ad esempio uno studente che può spostarsi solo a piedi. Inoltre potrebbe essere un possibile parametro iniziale su cui basiamo la visualizzazione dei lavori. Per la valutazione è stata scelta una scala da 1 a 5 (1=poco importante, 5=molto importante).

*Quanto reputi importante sapere indicativamente la retribuzione prima di candidarti per un lavoro?*: Il motivo per cui viene posta questa domanda è l'interesse nel comprendere quanto la retribuzione sia una priorità per il lavoratore. Potrebbero esserci più casi, ad esempio per uno studente che svolge ripetizioni ci si attende una retribuzione tendenzialmente standard, quindi in questo caso l'importanza potrebbe essere bassa, invece nel caso di un lavoratore altamente specializzato, come ad esempio un elettricista che deve svolgere un lavoro delicato l'importanza sarà più alta. Inoltre ci serve per capire se potrebbe essere un potenziale dato da aggiungere durante la fase di pubblicazione dell'offerta lavorativa (da chi offre il lavoro), così da poter essere visualizzata dai possibili candidati. Per la valutazione la scala va da 1 a 5 (1=poco importante, 5=molto importante).

*Come vorresti essere aggiornato sulle offerte di lavoro nella tua zona?*: Questa domanda ci permette di capire quali sono le modalità preferite dagli stakeholder per essere aggiornati su eventuali offerte di lavoro. Le possibili risposte sono 3:1.E-Mail 2.SMS 3.Sezione apposita dell'App(o notifica) oppure c'è la possibilità di inserire in una casella di testo un'altra opzione. Ognuna di queste risposte ci darà informazioni importanti riguardo al funzionamento del sistema, specie su come visualizzare il profilo dell'utente.

*Come vorresti segnalare eventuali problematiche? (Allergie,Handicap,...)*: La domanda viene posta per permettere agli utenti che presentano problematiche meno comuni di specificare come comunicare l'informazione all'eventuale offerente, evitando spiacevoli inconvenienti. Abbiamo pensato di inserire questa domanda per agevolare anche quelle categorie di persone che hanno maggiori problemi nello svolgere certe azioni (ad esempio allergie ad animali oppure l'impossibilità di salire le scale). Le possibili risposte sono 1.Chat all'interno dell'App 2.Casella di testo 3.E-Mail. Quindi sia privatamente che pubblicamente.

*Come vorresti visualizzare all'interno dell'applicazione gli annunci?* Il motivo della domanda è comprendere in quale modo gli stakeholder vogliono visualizzare le offerte, ovvero secondo una mappa (eg.TripAdvisor, Booking) quindi senza criterio di ordinamento oppure tramite Lista, in questo caso verrà mostrata un'ulteriore sezione del questionario. Non essendoci un criterio di ordinamento nel caso della mappa, sarà necessario specificare quali tipi di ordinamento sono più importanti per i nostri utenti (vedi domanda successiva). Le risposte possibili sono: 1.Mappa 2.Elenco/Lista 3.Entrambi.

*Quali sono per te i criteri di ordinamento più importanti nella visualizzazione degli annunci?* Questa domanda consente di comprendere quale sarà/quali saranno i criterii di ordinamento utilizzati, quindi ad esempio nel caso di "Retribuzione" il primo risultato della lista corrisponderà al pagamento più alto e l'ultimo al più basso. Questo requisito sarà particolarmente importante per il fine dell'applicazione, cioè l'immediatezza nello scegliere l'offerta migliore per l'utente. Sarà possibile scegliere una o più opzioni tra 1.Vicinanza 2.Lavoro 3.Retribuzione 4.Specificare altro.

#### Domande Generali

*Quanta importanza dai alla possibilità di scrivere una recensione su un altro profilo?* Questa domanda fornisce informazioni riguardo l'implementazione nell'App della possibilità di scrivere recensioni, in caso di una maggioranza di risposte con punteggio alto sarà possibile sfruttare questa funzionalità, altrimenti non verrà sviluppata. Questa domanda è anch'essa un po delicata, nel senso che non sempre le persone sono intenzionate a lasciare recensioni. Come tutte le domande di questa sezione è rivolta sia a chi cerca il lavoro sia a chi offre la possibilità di svolgere una mansione. La scala va da 1 a 5 (1=poco importante 5=molto importante)

*Quanta importanza dai alle recensioni altrui di solito?* Questo caso può sembrare simile al precedente, ma mentre nel primo la domanda riguardava una funzionalità aggiuntiva, in questo il quesito ci permetterà di capire quanto dare importanza agli utenti con recensioni migliori rispetto a quelli con recensioni peggiori, specie nel caso in cui siano equivalenti rispetto ai criteri di ordinamento. Sarà possibile scegliere un valore da 1 a 5 (1=poco importante 5=molto importante)

*Come vorresti accedere all'applicazione?* Requisito rigurdante la modalità di accesso all'applicazione, utile per comprendere quali tipi di account l'utente preferirebbe utilizzare, quindi ad esempio la E-Mail personale, più efficiente per la comunicazione o i social network, più immediati per effettuare una prima registrazione. Le possibili risposte sono 1.Social Network 2.E-Mail 3.Credenziali 4.Specificando un'altra opzione.

*Preferiresti pagare l'applicazione (piccolo contributo) oppure avere accesso gratuito ma con la visione di inserzioni pubblicitarie?* La risposta alla domanda ramificherà un'ultima volta il questionario, portando in entrambi i casi alla domanda conclusiva. In questo caso ci interessa anche capire se la maggior parte degli utenti preferisce dare un piccolo contributo oppure usufruire gratuitamente ma con la visione di inserzioni pubblicitarie. Le scelta ricadrà su 1.Pagamento 2.Pubblicità

*In che formato preferiresti vedere i messaggi pubblicitari?* In questo caso il potenziale utente avrà scelto "Pubblicità", si dovrà quindi decidere il formato, la risposta con più voti andrà a modificare il mockup dell'applicazione, ci aspettiamo che la maggior parte di queste risposte saranno date dal lavoratore, poichè meno propenso a spendere. Sarà possibile scegliere tra 1.Banner 2.Video.

*Come preferiresti pagare l'applicazione?* In questo caso il potenziale utente avrà scelto "Pagamento" sarà quindi necessario scgliere la modalità di pagamento della applicazione al momento del download dallo Store, è molto probabile che le risposte daranno una risposta a questa domanda gli utenti che offrono la possibilità di svolgere una mansione. Le eventuali risposte saranno 1.Carta di credito 2.Credito telefonico 3.Google Pay/Apple Pay/simili.

Link Questionario: https://docs.google.com/forms/d/1KTQjkQ8yhAn6Bs3okwtmvZ8_FYFzBcQID1gG1jw6xJ8/edit#responses


## Partecipanti
*Nome e Cognome*: __Fabio D'Adda__, *matricola*: __817279__, *e-mail*: __f.dadda4@campus.unimib.it__  
*Nome e Cognome*: __Mario Enrico Centrone__, *matricola*: __816325__, *e-mail*: __m.centrone2@campus.unimib.it__  
Link alla Repository: https://gitlab.com/Fabio597/2020_assignment2_daily_job