## Descrizione Progetto

Il nostro progetto consiste nel realizzare un'applicazione chiamata **Daily Job**, il cui scopo è quello di permettere agli utenti di sfruttare il proprio tempo libero e trasformarlo in una possibilità di guadagno. L'idea è di offrire l'opportunità di un lavoro occasionale a chi ne ha bisogno, andando a facilitare la fase di ricerca e comunicazione con chi ha necessità di una qualche forma di manodopera. Il sistema quindi va ad agevolare il matching tra domanda e offerta per lavori occasionali. L'intento è quindi far incontrare le due parti per poi lasciare a loro le modalità che preferiscono per accordarsi effettivamente sugli aspetti concreti dell'erogazione del servizio.
L'applicazione sarà quindi in grado di:

* Mostrare i vari lavori disponibili in una certa provincia e/o in generale in una certa zona Geografica, consentendo di visualizzarli attraverso alcuni criteri.
* Permettere a chi ha bisogno di ricevere una prestazione lavorativa di specificare la mansione, il giorno e la fascia oraria.
* Consentire a chi vorrebbe fare un lavoretto di poter cercare tra le eventuali offerte presenti.
* Chi poi risulta effettivamente interessato ad uno dei lavori presenti può, sempre attraverso l'applicazione, candidarsi per erogare effettivamente il servizio.

Sosteniamo come requisito fondamentale l'affidabilità e la sicurezza, motivo per cui viene implementato un sistema di recensioni simile ad altri servizi già diffusi (ex. TripAdvisor).

Questo documento si pone come obbiettivo quello di stabilire quali requisiti questo sistema deve soddisfare per garantire la corretta interpretazione e lo sviluppo del sistema da sviluppare. Per questo attraverso alcune attività andremo a coinvolgere alcuni stakeholder (_coloro che hanno un interesse nello sviluppo del progetto_) e a scoprire i requisiti che in fase di implementazione dovranno poi essere rispettati dal sistema software. 