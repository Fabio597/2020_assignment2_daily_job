## Mock-Up Applicazione

In questa fase verrano mostrati dei mock-up che rispetteranno i requisiti che sono stati ricavati e di conseguenza definiti in precedenza. L'obbiettivo di questa fase del workflow è quella di mostrare le varie funzionalità e alcuni aspetti dell'applicazione. Abbiamo posizionato questa fase dopo il questionario e soprattutto dopo la valutazione delle risposte perchè i mock-up sono stati creati sulla base delle risposte che sono state date dagli stakeholder (Nel nostro caso un campione degli utenti finali) nel questionario che è stato somministrato. Le varie funzionalità che mostreremo e spiegheremo attraverso l'uso dei Mock-Up sono sostanzialmente i requisiti che sono stati individuati e specificati nella sezione precedente.
In questa fase Identifichiamo come stakeholder coinvolti:

* __Il team di sviluppo__: Il team di sviluppo in questa fase viene coinvolto in quanto sono
esperti in vari anche più tecnici tra cui: utilità, interfaccia grafica e efficienza. La loro valutazione risulta essere più tecnica rispetto al campione di utenti che viene selezionato.
* __Un campione di utenti__: Viene coinvolto un campione di utenti per valutare l'utilità e l'usabilità delle funzionalità dell'applicazione.

Prima di procedere con le schermate, è possibile (e consigliato) vedere il flusso dell'applicazione al seguente link: https://marvelapp.com/prototype/544a18e/screen/74733642.

Prendendo visione del flusso, sarà più comprensibile capire anche le schermate che saranno spiegate successivamente.

1. _Mostrare il prezzo indicativo dei lavoratori, stimato come prezzo all'ora, da poter visualizzare quando compare la lista dei candidati per una determinata mansione_.
   
    In questa schermata è possibile visualizzare il prezzo indicativo dei lavoratori a destra in ogni elemento della lista che viene visualizzata, per riuscire a dare un'idea indicativa di quanto l'Utilizzatore è intenzionato a pagare il Prestatore. Il prezzo, essendo indicativo, non è fisso, per questo è possibile che una volta che le parti si siano messe in contatto, ci si può accordare sul prezzo.
    
    ![image info](./Mockup/ListaLavori.png)
    
2. _Mostrare la specializzazione del lavoratore all'interno dell'annuncio per far capire a chi sta offrendo il lavoro quanto il candidato sia esperto nel lavoro da svolgere._
   
    In questo caso è possibile vedere all'interno della pagina del dettaglio del candidato le esperienze che ha svolto nella mansione richiesta dall'annuncio di lavoro.
    
    Inoltre da questa schermata è possibile mostrare anche il funzionamento di un altro requisito specificato sopra: _Il candidato deve poter essere contattato da chi offre il lavoro attraverso: Numero di Telefono, E-Mail e Chat_. Si vede infatti che è possibile contattare il candidato attraverso le tre modalità:
    * __E-Mail__: Premendo il bottone rosso "Contatta via E-Mail"
    * __Chat__: Premendo il bottone verde "Apri Chat"
    * __Numero di Telefono__: Premendo il bottone blu"Chiama"
    
    ![image info](./Mockup/DettaglioCandidato.png)
    
    Mostriamo anche come è fatta la chat tra l'Utilizzatore e il Prestatore (premendo il tasto per aprire la chat), attraverso il seguente Mock-up:
    
    ![image info](./Mockup/Chat.png)
    
3. _Non deve essere necessario specificare con esattezza la data e l'ora dell'inizio della prestazione lavorativa._
   
    Nella lista degli annunci non sono necessariamente presenti data e ora dell'inizio della prestazione lavorativa, ma sono specificati solo se l'Utilizzatore ha necessità di precisarli.
    
    ![image info](./Mockup/ElencoAnnunci.png)
    
    Come è possibile vedere in questa schermata al momento della pubblicazione dell'annuncio non sono informazioni obbligatorie da inserire.
    
    ![image info](./Mockup/CreaAnnuncio.png)
    
    
4. _Deve essere possibile vedere quanto (indicativamente) è retribuito un lavoro, prima di potersi candidare effettivamente._
   
    Nella lista degli annunci è possibile vedere la retribuzione di un determinato lavoro (sempre stimata all'ora) prima di potersi candidare. Anche se la retribuzione non è fissa, almeno è possibile avere un'idea della paga che l'utilizzatore ha specificato. Sarà possibile poi dopo essersi candidati la possibilità di accordarsi direttamente.
    
    ![image info](./Mockup/FiltroLavoro.png)
    
5. _Mostrare attraverso una notifica e con l'invio di una E-Mail la pubblicazione di un nuovo lavoro nelle vicinanze o di una mansione che si è interessati a svolgere._
   Quando viene pubblicato un'offerta di lavoro che potrebbe interessare ad un utente, magari perchè è nelle vicinanze e/o risulta essere un lavoro interessante per l'utente, viene inviata una notifica dall'applicazione per segnalare l'annuncio:
   
   ![image info](./Mockup/NotificaApp.png)
   
   Se in fase di registrazione è stata anche specificata una mail, verrà inviata una comunicazione anche per posta elettronica:
   
   ![image info](./Mockup/NotificaMail.png)
   
6. _Possibilità di segnalare problematiche attraverso una o più fra queste modalità: Chat interna all'app, Casella di testo o E-Mail_.
	
	In questa schermata è possibile segnalare attraverso una __casella di testo__ se ci dovessero essere eventuali problematiche da affrontare prima dell'inizio del lavoro:
	
	![image info](./Mockup/Candidatura.png)

Analogamente alla casella di testo, sono state progettate anche le situazioni in cui un candidato può segnalare sempre eventuali problematiche attraverso altre due modalità che sono: __Chat interna all'app__ e __E-Mail__. In questo caso per poter chattare e/o inviare una mail per segnalare alcune problematiche, è necessario effettuare la candidatura e dopodiché entrare nel dettaglio della candidatura stessa per accedere alla chat e/o inviare una mail:

![image info](./Mockup/DettaglioCandidatura.png)

7. _Visualizzazione degli annunci attraverso un elenco e una mappa_

	In questa schermata è possibile visualizzare __l'elenco__ degli annunci (ordinati di default per __vicinanza__, essendo il criterio di ordinamento più votato dagli utenti) e attraverso il componente di "segmeted control" c'è la possibilità di passare alla modalità __mappa__. Di seguito vengono mostrate entrambe le modalità:

![image info](./Mockup/ElencoAnnunci.png)

![image info](./Mockup/Mappa.png)

Queste due modalità sono state inserite nei Mockup perchè nelle risposte del questionario la maggior parte degli utenti ha specificato di voler visualizzare gli annunci in entrambi i modi.

8. _La lista degli annunci deve poter essere ordinata/filtrata secondo questi criteri: Vicinanza, Retribuzione e Lavoro_

Come si può vedere dalla schermata precedente, la lista è già ordinata di default per vicinanza (essendo l'opzione migliore secondo gli stakeholder). Inoltre è possibile ordinare gli annunci di lavoro cliccando sul bottone in alto a destra __Ordina Per__ e successivamente si aprirà la seguente schermata:

![image info](./Mockup/SchermataOrdina.png)

Mostriamo di seguito l'ordinamento dei lavori per retribuzione (dal più alto al più basso), applicabile premendo il filtro __Retribuzione__ presente:

![image info](./Mockup/OrdinamentoRetribuzione.png)

E infine il filtro per uno o più __lavori__ specifici che possono essere selezionati come di seguito:

![image info](./Mockup/SceltaFiltroLavoro.png)

Per poi visualizzare la lista dei __lavori__ _filtrati_ per il lavoro scelto nella schermata precedente:

![image info](./Mockup/FiltroLavoro.png)

9. _Possibilità di lasciare una recensione per entrambe le parti (lavoratore e chi ha offerto il lavoro) alla fine della prestazione lavorativa_.

Alla fine dell'attività lavorativa, chi ha offerto il lavoro, può chiudere l'annuncio andando nel dettaglio dell'annuncio che vuole chiudere perchè l'attività lavorativa è terminata:

![image info](./Mockup/DettaglioAnnuncio.png)

Una volta chiuso l'annuncio è possibile lasciare una recensione dato che apparirà la schermata:

![image info](./Mockup/LasciaRecensioneUtilizzatore.png)

Come si può notare, è possibile anche saltare questa opzione e chiudere definitivamente l'annuncio di lavoro.
Una volta chiusa l'offerta il lavoratore verrà notificato e anche lui potrà accedere alla sezione dei lavori per cui si è candidato e scrivere (se lo desidera) una recensione.

Una volta terminata l'attività lavorativa, allo stesso modo il Prestatore (chi ha lavorato) potrà scrivere una recensione nei confronti dell'utilizzatore.

10. _Visualizzazione delle Recensioni in fase di candidatura e di selezione_

Per quanto riguarda questo requisito mostriamo di seguito come entrambe le parti possono consultare le recensioni:

* __Prestatore__:

![image info](./Mockup/Candidatura.png)

Il candidato prima di confermare la candidatura per il lavoro, può vedere le recensioni associate al profilo di chi sta offrendo il lavoro. In questo caso verranno mostrate le recensioni  legate al profilo, ma solo quelle recensioni lasciate quando ha offerto qualche lavoro in passato (Se dovesse essere anche un lavoratore, non verranno mostrate le recensioni legate ai suoi lavori, questo perchè ricordiamo che ogni profilo può avere due tipi di recensioni: Recensioni Utilizzatore e Recensioni Prestatore).
Premendo sul link per visualizzare le recensioni, verrà visualizzata la schermata:

![image info](./Mockup/RecensioniUtilizzatore.png)

In questa schermata mostriamo la lista di recensioni lasciate dai lavoratori per eventuali lavori svolti in passato per conto del signor Franco Bassi.

* __Utilizzatore__:

![image info](./Mockup/IMieiAnnunci.png)

L'utilizzatore, per consultare le recensioni dei candidati, può selezionare dalla lista dei suoi annunci, andando nel dettaglio dell'annuncio stesso, i candidati che fino a quel momento si sono candidati per svolgere il lavoro.

![image info](./Mockup/IMieiAnnunciDettaglio.png)

Per ogni candidato è possibile accedere ai dettagli della sua candidatura:

![image info](./Mockup/DettaglioCandidato.png)

Una volta all'interno del dettaglio del candidato, si possono visualizzare le recensioni associate ai lavori svolti premendo sul bottone giallo: "Vedi Recensioni" (Si ricorda che vengono visualizzate solo le Recensioni legate alle prestazioni lavorative di Valentina Verdi, quindi non verrà visualizzata nessuna recensione riguardo agli eventuali lavori offerti da Valentina):

![image info](./Mockup/RecensioniPrestatore.png)

11. _Modalità per accedere all'applicazione: E-Mail e/o Credenziali_

![image info](./Mockup/LoginPage.png)

Per accedere all'applicazione, è necessario autenticarsi tramite __E-Mail__ oppure tramite __Credenziali__.
* __E-Mail__: Accedere premendo il tasto "_Sign in with E-Mail_"
* __Credenziali__: Inserire le credenziali e premere il tasto "_Sign in with Credentials_"
Se la registrazione non è ancora stata effettuata, basterà premere sul link "Registrati" per poter creare un account:

![image info](./Mockup/SchermataRegistrazione.png)

In questa schermata è possibile registrarsi sia tramite credenziali, sia tramite E-Mail.

12. _Visualizzazione di messaggi pubblicitari tramite Banner_

![image info](./Mockup/ListaLavori.png)

In questo mock-up mostriamo come verrano presentate le inserzioni pubblicitarie nelle schermate. Dalle risposte raccolte, abbiamo visto che gli stakeholder preferiscono vedere le pubblicità tramite __Banner__ (come mostrato sopra.)